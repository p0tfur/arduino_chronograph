// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins (rs, E,  D4, D5, D6, D7)
LiquidCrystal lcd(4, 5, 6, 7, 8, 9);

unsigned long time1;
unsigned long time2;
int pin_rec_1 = A7;
int pin_rec_2 = A5;
int pin_trans_1 = A0;
int pin_trans_2 = A2;
float fps, sum, ms, fps2;

void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);

  Serial.begin(9600);
  pinMode(pin_rec_1, INPUT_PULLUP); 
  pinMode(pin_rec_2, INPUT_PULLUP); 
  //pinMode(pin_rec_2, INPUT_PULLUP); 
  
  pinMode(pin_trans_1, OUTPUT);
  pinMode(pin_trans_2, OUTPUT);

  analogWrite(pin_trans_1, 255); //dioda ma sie swiecic ciagle
  analogWrite(pin_trans_2, 255);

  time1 = 0;
  time2 = 0;
}

void loop() 
{
  lcd.setCursor(0, 0);
  lcd.clear();
  lcd.print("Ready!");

  //Serial.println(F("jeden"));
  //Serial.println(analogRead(pin_rec_1));
  //Serial.println(F("dwa"));
  //Serial.println(analogRead(pin_rec_2));

  while(analogRead(pin_rec_1) < 5); //LOW
  
  while(analogRead(pin_rec_1));
  Serial.println(analogRead(pin_rec_1));
  time1 = micros();   
   
  while(analogRead(pin_rec_2) > 700);
  Serial.println(analogRead(pin_rec_2));
  time2 = micros(); 
  
  call_effect();

}

void call_effect()
{
  sum = (time2 - time1);
  ms = (0.0618 / sum);
  sum = 0.0618/(sum/1000000);
  fps = sum * 3.2808;
  fps2 = ms * 3300000;
  lcd.setCursor(0, 1);
  lcd.print(fps); 
  lcd.setCursor(6, 1);
  lcd.print("fps");
  lcd.setCursor(10, 1);
  lcd.print(fps2);
  delay(3000);
}
